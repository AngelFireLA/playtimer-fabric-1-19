package be.bjarno.playtimer;

import static net.fabricmc.fabric.api.client.command.v2.ClientCommandManager.literal;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import be.bjarno.playtimer.commands.HelpCommand;
import be.bjarno.playtimer.commands.ResetCommand;
import be.bjarno.playtimer.commands.SyncCommand;
import be.bjarno.playtimer.commands.ToggleCommand;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.command.v2.ClientCommandManager;

public class PlaytimerMod implements ModInitializer {
	// This logger is used to write text to the console and the log file.
	// It is considered best practice to use your mod id as the logger's name.
	// That way, it's clear which mod wrote info, warnings, and errors.
	public static final Logger LOGGER = LoggerFactory.getLogger("Playtimer");

	private static File hiddenfile = new File(".playtimer-hidden");
	static boolean timerVisible = true;

	@Override
	public void onInitialize() {
		Storage storage = Storage.getInstance();
		storage.readFile();
		LOGGER.debug("Playtimes loaded...");

		ClientCommandManager.getActiveDispatcher().register(
				literal("playtimer")
						.then(literal("reset").executes(new ResetCommand()))
						.then(literal("sync").executes(new SyncCommand()))
						.then(literal("toggle").executes(new ToggleCommand()))
						.then(literal("help").executes(new HelpCommand()))
						.executes(new HelpCommand())
		);

		loadVisible();
	}

	public static boolean toggle() {
		timerVisible = !timerVisible;
		saveVisible();
		return timerVisible;
	}

	public static void loadVisible() {
		timerVisible = !hiddenfile.exists();
	}

	public static void saveVisible() {
		if (timerVisible) {
			if (hiddenfile.exists()) {
				hiddenfile.delete();
			}
		} else {
			if (!hiddenfile.exists()) {
				try {
					hiddenfile.createNewFile();
				} catch (IOException e) {
					LOGGER.error("Could not update timer visibility status...");
				}
			}
		}
	}

}
