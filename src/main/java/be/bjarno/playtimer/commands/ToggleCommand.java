package be.bjarno.playtimer.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;

import be.bjarno.playtimer.PlaytimerMod;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.minecraft.text.Text;

public class ToggleCommand implements Command<FabricClientCommandSource> {
    @Override
    public int run(CommandContext<FabricClientCommandSource> context) {
        FabricClientCommandSource source = context.getSource();
        boolean newStatus = PlaytimerMod.toggle();

        if (newStatus) {
            source.sendFeedback(Text.literal("playtimer.show"));
        } else {
            source.sendFeedback(Text.literal("playtimer.hide"));
        }

        return 0;
    }
}
